class AddContentToMailTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :mail_templates, :content, :text
  end
end
