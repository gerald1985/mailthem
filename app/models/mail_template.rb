class MailTemplate < ApplicationRecord
	require 'csv'
	attr_accessor :mail_list, :csv
	validates_uniqueness_of :subject
	validates_presence_of :subject, :content
	include Bootsy::Container
	
	def readFile
		list = Array.new
		CSV.foreach(csv.path, headers: true) do |row|
			list.push(row.to_s.gsub("\n","").split.join)
		end
		list
	end
end
