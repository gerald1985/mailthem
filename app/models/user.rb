class User < ApplicationRecord
	mount_uploader :avatar, AvatarUploader
	include UserModelUtility
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
		:recoverable, :rememberable, :trackable, :validatable

	validates_uniqueness_of :email
end
