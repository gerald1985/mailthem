module ApplicationHelper

	def current_page(controller_name)
		if (controller.class==controller_name)
			"current-page"
		end
	end

	def footer_menu
		items = [left_link, middle_left_link, middle_right_link, right_link]
		content_tag :div, :class => "sidebar-footer hidden-small" do
			items.collect { |item| concat item}
		end
	end
	def left_link
		link_to(content_tag(:span, nil, :class=>"glyphicon", :"aria-hidden"=>"true"), 
			nil, :"data-toggle"=>"", :"data-placement"=>"", :title=>'')
	end
	def middle_left_link
		left_link
	end
	def middle_right_link
		left_link
	end
	def right_link
		link_to(content_tag(:span, nil, :class=>"glyphicon glyphicon-off", :"aria-hidden"=>"true"), 
			end_session_path, :class=>"link", :"data-toggle"=>"tooltip", :"data-placement"=>"top", :title=>'Log Out', :method => :delete )
	end

	def top_nav_menu
		content_tag :div, :class => "top_nav" do
			content_tag :div, :class => "nav_menu" do
				content_tag :nav, :role => "navigation" do
					bar_link = content_tag :div, :class=>"nav toggle" do
						content_tag :a, :id => "menu_toggle" do
							content_tag(:i,nil,:class=>"fa fa-bars")
						end
					end
					menu_container = content_tag :ul, :class=> "nav navbar-nav navbar-right" do
						concat user_menu
					end
					concat bar_link
					concat menu_container
				end
			end
		end
	end

	def user_menu
		content_tag :li do
			content_container = ActiveSupport::SafeBuffer.new
			internal_content_container = ActiveSupport::SafeBuffer.new
			content_container << link_to("#", :class=>"user-profile dropdown-toggle", :"data-toggle"=>"dropdown", :"aria-expanded"=>"false") do
				concat image_tag(current_user.image_source("avatar"), "aria-hidden"=>"false")
				concat " " + current_user.name.to_s + " "
				concat content_tag(:span, nil, :class=>"fa fa-angle-down")
			end

			content_container << content_tag(:ul, :class=>"dropdown-menu dropdown-usermenu pull-right") do
				internal_content_container << content_tag(:li) do
					concat link_to("Profile", edit_user_path)
				end
				#internal_content_container << content_tag(:li) do
				#	link_to("Settings", "#")
				#end
				#internal_content_container << content_tag(:li) do
				#	link_to("Help", "#")
				#end
				internal_content_container << content_tag(:li) do
					link_to(content_tag(:i, nil, :class=>"fa fa-sign-out pull-right") + " Log Out", 
						end_session_path, :method => :delete)
				end
				internal_content_container
			end
			content_container
		end
	end
end
