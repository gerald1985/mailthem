class TemplateMailer < ApplicationMailer
	default from: 'notify@mysite.com'

	def self.template_sender(emailList, subject, template)
		emailList.each do |email|
			if TemplateMailer.is_a_valid_email?(email)
				rich_text_template(email, subject, template).deliver_now
			end
		end
	end

  	def rich_text_template(email, subject, template)
  		@template = template.html_safe
    	mail(to: email, subject: subject)
	end

	def self.is_a_valid_email?(email)
		puts ">>>>> #{email}"
		special = "?<>',?[]}{=-)(*&^%$#`~{}"
		regex = /[#{special.gsub(/./){|char| "\\#{char}"}}]/
		if email.count("@") != 1 then
			puts ">>>>> #{email} format invalid "
			return false
		elsif email =~ regex
			puts ">>>>> #{email} format invalid "
			return false
		elsif email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i then
			puts ">>>>> #{email} valid "
			return true
		else
			return false
		end
	end
end
