// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootsy
//= require jquery-fileupload/basic
//= require_tree .
//= require gentelella
//= require gentelella-custom

/*TURBO LINKS WERE REMOVED require turbolinks*/
$(document).ready(function(){	
	// Select the submit buttons of forms with data-confirm attribute
	var submit_buttons = $("form[data-confirm] a#delete_records.submit-form");
	// On click of one of these submit buttons
	submit_buttons.on('click', function (e) {

		// Prevent the form to be submitted
		e.preventDefault();

		var button = $(this); // Get the button
		var form = button.closest('form'); // Get the related form
		var msg = form.data('confirm'); // Get the confirm message

		if(confirm(msg)) form.submit(); // If the user confirm, submit the form
	});
});