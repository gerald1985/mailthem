class Users::RegistrationsController < Devise::RegistrationsController
  before_action :set_page_title
  before_action :authenticate_user!, only: [:show, :edit, :update, :destroy]
  helper_method :end_session_path, :session_root_path, :edit_user_path
  layout :user_layout
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
     super
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  def controller?(*controller)
      controller.include?(params[:controller])
  end

  def action?(*action)
      action.include?(params[:action])
  end

  private

  def user_layout
    current_user.present? ? "application" : "login_layout"
  end

  def set_page_title
      @page_title = "Registry Management"
  end

  def end_session_path
      destroy_user_session_path
  end

  def session_root_path
      root_path
  end

  def edit_cie_user_path
      edit_user_registration_path
  end

  protected

  def after_update_path_for(resource)
      edit_user_registration_path(resource)
  end
end
