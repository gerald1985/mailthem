class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	before_action :set_page_title, :authenticate_user!
	before_action :configure_account_update_params, if: :devise_controller?
	skip_before_action :verify_authenticity_token, if: :devise_controller?
	helper_method :end_session_path, :session_root_path, :edit_user_path

  	def configure_account_update_params
		devise_parameter_sanitizer.permit(:account_update, keys: [:name, :avatar])
	end
	def controller?(*controller)
		controller.include?(params[:controller])
	end

	def action?(*action)
		action.include?(params[:action])
	end
	def set_page_title
		@page_title = "MailThem Platform"
	end
	def session_root_path
		root_path
	end
	def end_session_path
		destroy_user_session_path
	end
	def edit_user_path
		edit_user_registration_path
	end
end
