class DashboardController < ApplicationController
  def index
    @mail_templates = MailTemplate.all.order(:subject)
  end
end
