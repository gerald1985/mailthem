Rails.application.routes.draw do
	mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	devise_for :users, class_name: "User",
	:controllers => { :registrations => 'users/registrations', :sessions => 'users/sessions', :passwords => 'users/passwords' },
	path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'sign_up' }
	root :to => "dashboard#index"
	resources :mail_templates do
		collection do
			delete 'destroy_multiple'
		end
	end
	get "send_mail_template/:id", to: 'mail_templates#send_mail_template', as: :send_mail_template
	patch "send_csv_mail_template/:id", to: 'mail_templates#send_csv_mail_template', as: :send_csv_mail_template
end
