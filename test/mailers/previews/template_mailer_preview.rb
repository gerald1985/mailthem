# Preview all emails at http://localhost:3000/rails/mailers/template_mailer
class TemplateMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/template_mailer/rich_text_template
  def rich_text_template
    TemplateMailer.rich_text_template
  end

end
