require 'test_helper'

class TemplateMailerTest < ActionMailer::TestCase
  test "rich_text_template" do
    mail = TemplateMailer.rich_text_template
    assert_equal "Rich text template", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
